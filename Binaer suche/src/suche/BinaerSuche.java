package suche;

public class BinaerSuche {


		public static int BSuche(long [] array,int arraylaenge, long suchwert){
		    final int NICHTGEFUNDEN = -1;
			int mitte;
		    int links = 0;                      
		    int rechts = arraylaenge - 1;         

		   
		    while (links <= rechts){
		        mitte = links + ((rechts - links) / 2); 

		        if (array[mitte] == suchwert)       
		            return mitte;
		        else
		            if (array[mitte] > suchwert)
		                rechts = mitte - 1;     
		            else 
		                links = mitte + 1;      
		    }

		    return NICHTGEFUNDEN;
		    
		}
	}

		
		

