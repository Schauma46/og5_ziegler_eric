package primzahlen;

public class IsPrim {

	public static boolean isPrimzahl(long zahl) {
		boolean ist = true;
		if (zahl < 2) {
			ist = false;
		} // end of if
		else {
			for (long i = 2; i <= (zahl / 2) + 1; i++) {
				if (zahl % i == 0) {
					ist = false;
					break;
				} // end of if
			}
		} // end of if-else

		return ist;
	}

}
