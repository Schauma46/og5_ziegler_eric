package primzahlen;

public class StoppUhr {

	//Attribute 
		private long startpunkt;
		private long endpunkt;

	//Methoden
		public StoppUhr(){
			
		}
		public void Starten(){
			startpunkt = System.currentTimeMillis();
		}
		public void Stoppen(){
			endpunkt = System.currentTimeMillis();
		}
		public void Reset(){
			startpunkt = 0;
			endpunkt = 0;
		}
		public long getDauer(){
			return endpunkt - startpunkt;
		}	
}
