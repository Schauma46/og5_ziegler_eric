package primzahlen;

import java.util.Scanner;

public class PrimTest {

	public static void main(String[] args) {

		// Attribute
		long prim = 0;

		StoppUhr stoppen = new StoppUhr();
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie eine Primzahl ein!");
		prim = scan.nextLong();

		stoppen.Starten();
		boolean out = IsPrim.isPrimzahl(prim);
		stoppen.Stoppen();
		System.out.println("IsPrime = " + out);
		System.out.println("");
		System.out.println("Die Schleife hat: " + stoppen.getDauer() + " millisec gebraucht");
		System.out.println("Die Schleife hat: " + stoppen.getDauer() / 1000.0 + " sekunden gebraucht");
		System.out.println("Die Schleife hat: " + stoppen.getDauer() / 60000.0 + " minuten gebraucht");
		scan.close();
	}
}
