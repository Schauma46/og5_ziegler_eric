package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import data.Kunde;
import data.KundenListe;

public class MySQLDatabase {

	/**
	 * ein neuer Kunde soll mit einem INSERT Befehl eingetragen werden
	 * @param k - der Kunde
	 * @return true wenn die Aktion erfolgreich war
	 */
	public boolean neuerKunde(Kunde k) {
		try {
		// Parameter f�r Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:82/velo";
		String user = "root";
		String password = "";
		// JDBC-Treiber laden
		Class.forName(driver);
		// Verbindung aufbauen
		Connection con;
		con = DriverManager.getConnection(url, user, password);
		// Zeichenkette f�r SQL-Befehl erstellen
		String sql = "INSERT INTO TKunden (p_kundennummer, vorname, nachname, strasse, plz, ort, telefonnummer) VALUES('" + k.getKundennummer() + "', '" + k.getVorname() + "', '" + k.getNachname() + "', '" + k.getStrasse() + "', '" + k.getPlz() + "', '" + k.getOrt() + "', '" + k.getTelefonnummer() + "');";
		// Statement erstellen
		Statement stmt = con.createStatement();
		// SQL-Anweisungen ausf�hren
		stmt.executeUpdate(sql);
		// Verbindung schlie�en
		con.close();
		} catch (Exception ex) { // Fehler abfangen
		ex.printStackTrace();// Fehlermeldung ausgeben
		return false;
		}
		return true;
		}

	/**
	 * es soll die höchste Kundennummer der Datenbank aus der Kundentabelle
	 * zurückgegeben werden
	 * @return die höchste Kundennummer
	 */
	public int maxKNr() {
		int maxKNr = 0;
		try {
		// Parameter f�r Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:82/velo";
		String user = "root";
		String password = "";
		// JDBC-Treiber laden
		Class.forName(driver);
		// Verbindung aufbauen
		Connection con;
		con = DriverManager.getConnection(url, user, password);
		// Zeichenkette f�r SQL-Befehl erstellen
		String sql = "SELECT MAX(p_kundennummer) FROM TKunden";
		// Statement erstellen
		Statement stmt = con.createStatement();
		// SQL-Anweisungen ausf�hren
		ResultSet rs = stmt.executeQuery(sql);
		// ResultSet auswerten
		if (rs.next())
		maxKNr = rs.getInt(1);
		// Verbindung schlie�en
		con.close();
		} catch (Exception ex) {
		ex.printStackTrace();
		}
		return maxKNr;
		}

	/**
	 * Es soll die komplette Kundenliste zurückgegeben werden, nach Nachnamen, Vornamen sortiert
	 * @return sortierte Kundenliste
	 */
	public KundenListe getKundenliste() {
		KundenListe klist = new KundenListe();
		try {
		// Parameter f�r Verbindungsaufbau definieren
		String driver = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:82/velo";
		String user = "root";
		String password = "";
		// JDBC-Treiber laden
		Class.forName(driver);
		// Verbindung aufbauen
		Connection con;
		con = DriverManager.getConnection(url, user, password);
		// Zeichenkette f�r SQL-Befehl erstellen
		String sql = "SELECT p_kundennummer, vorname, nachname, strasse, plz, ort, telefonnummer FROM TKunden ORDER BY nachname, vorname;";
		// Statement erstellen
		Statement stmt = con.createStatement();
		// SQL-Anweisungen ausf�hren
		ResultSet rs = stmt.executeQuery(sql);
		// ResultSet auswerten
		while (rs.next()) {
		Kunde k = new Kunde(rs.getInt("p_kundennummer"), rs.getString("vorname"), rs.getString("nachname"), rs.getString("strasse"), rs.getString("plz"), rs.getString("ort"), rs.getString("telefonnummer"));
		klist.addKunde(k);
		}

			// ResultSet auswerten
			while (rs.next()) {
				Kunde k = new Kunde(rs.getInt("p_kundennummer"), rs.getString("vorname"), rs.getString("nachname"),
						rs.getString("strasse"), rs.getString("plz"), rs.getString("ort"),
						rs.getString("telefonnummer"));
				klist.addKunde(k);
			}
			// Verbindung schließen
			con.close();
		} catch (Exception ex) {
		ex.printStackTrace();
		}
		return klist; 
		}

	// ------------------------ FINGER WEG VON DIESEM TEIL DES QUELLCODES ----------------------------------------------------------

	/**
	 * Erstellt ggf. eine Datenbank mit Beispieldaten 
	 * Voraussetzung: MySQL-Datenbank gestartet 
	 * FINGER WEG VON DIESEM TEIL DES QUELLCODES
	 */
	public void setup() {
		try {
			// Parameter für Verbindungsaufbau definieren
			String driver = "com.mysql.jdbc.Driver";
			String url = "jdbc:mysql://localhost/?";
			String user = "root";
			String password = "";
			// JDBC-Treiber laden
			Class.forName(driver);
			// Verbindung aufbauen
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			// SQL-Anweisungen ausführen
			Statement stmt = con.createStatement();
			stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS velocebello;");
			// Verbindung schließen
			con.close();
			url = "jdbc:mysql://localhost/velocebello?";
			// Verbindung aufbauen
			con = DriverManager.getConnection(url, user, password);
			stmt = con.createStatement();
			// Kundentabelle einfügen
			stmt.executeUpdate("CREATE TABLE IF NOT EXISTS TKunden(" + "p_kundennummer INT PRIMARY KEY,"
					+ "vorname VARCHAR(50) NOT NULL," + "nachname VARCHAR(50) NOT NULL," + "strasse VARCHAR(50),"
					+ "plz CHAR(5)," + "ort VARCHAR(30)," + "telefonnummer VARCHAR(25));");
			// prüfen, ob DB bereits existiert und gefüllt ist
			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM TKunden;");
			boolean insert = true;
			if (rs.next())
				insert = rs.getInt(1) == 0;
			// ggf. Startdatensätze einfügen
			if (insert) {
				stmt.executeUpdate(
						"INSERT INTO TKunden VALUES (1,'Jan', 'Ullrich', 'Apothekengasse 14', '63254', 'Regensburg', '0232 258699884'),"
								+ "(2,'Lance', 'Armstrong', 'Dopingallee 26', '43215', 'Düsseldorf', '0489 2547896'),"
								+ "(3,'Ries', 'Bjarne', 'Epostraße 5', '16589', 'Prenzlau', '0772 12546663')");
			}
			con.close();

		} catch (Exception ex) { // Fehler abfangen
			ex.printStackTrace();// Fehlermeldung ausgeben
		}
	}

}
