package Stoppuhr;

public class StoppuhrTest {

public static void main(String [] Args){

	Stoppuhr Stoppuhr = new Stoppuhr();
	Stoppuhr.Starten();
	for(int i = 1; i <= 1000000; i++){
		System.out.println(i);
		if(i == 1000000){
			Stoppuhr.Stoppen();
		}
	}
	System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht.");
	System.out.println("Die Schleife hat: " + Stoppuhr.getDauer()/ 1000.0 + " sekunden gebraucht");
	System.out.println("Die Schleife hat: " + Stoppuhr.getDauer()/ 60000.0 + " minuten gebraucht");
	System.out.println("Die Schleife hat: " + Stoppuhr.getDauer()/ 1000.0 / 60.0 / 60.0 + " Stunden gebraucht");
}
}
