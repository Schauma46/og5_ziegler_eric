package de.oszimt.starsim2099;

/**
 * Write a description of class Planet here.
 * 
 * @author (Eric Ziegler)
 * @version (25.09.2016)
 */
public class Planet {

	// Attribute
	private int posX;
	private int posY;
	private int anzahlHafen;
	private String name;
	
	// Methoden
	public Planet(){
		
	}
    public void setPosX(int x){
    	this.posX = x;
    }
    public int getPosX(){
    	return this.posX;
    }
    public void setPosY(int y){
    	this.posY = y;
    }
    public int getPosY(){
    	return this.posY;
    }
    public void setAnzahlHafen(int h){
    	this.anzahlHafen = h;
    }
    public int getAnzahlHafen(){
    	return this.anzahlHafen;
    }
    public void setName(String n){
    	this.name = n;
    }
    public String getName(){
    	return this.name;
    }
    
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] planetShape = { { '\0', '/', '*', '*', '\\', '\0' }, { '|', '*', '*', '*', '*', '|' },
				{ '\0', '\\', '*', '*', '/', '\0' } };
		return planetShape;

	}
}
