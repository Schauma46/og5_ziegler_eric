package de.oszimt.starsim2099;

/**
 * Write a description of class Pilot here.
 * 
 * @author (Eric Ziegler)
 * @version (25.09.2016)
 */
public class Pilot {

	// Attribute
	private int posX;
	private int posY;
	private String grad;
	private String name;
	
	// Methoden
	public Pilot(){
		
	}
    public void setPosX(int x){
    	this.posX = x;
    }
    public int getPosX(){
    	return this.posX;
    }
    public void setPosY(int y){
    	this.posY = y;
    }
    public int getPosY(){
    	return this.posY;
    }
    public void setGrad(String g){
    	this.grad = g;
    }
    public String getGrad(){
    	return this.grad;
    }
    public void setName(String n){
    	this.name = n;
    }
    public String getName(){
    	return this.name;
    }
}
