package de.oszimt.starsim2099;

/**
 * Write a description of class Raumschiff here.
 * 
 * @author (Eric Ziegler)
 * @version (25.09.2016)
 */
public class Raumschiff {

	// Attribute
	private int posX;
	private int posY;
	private int maxLadekapazitaet;
	private String typ;
	private String antrieb;
	private int winkel;
	
	// Methoden
    public Raumschiff(){
    	
    }
    public void setPosX(int x){
    	this.posX = x;
    }
    public int getPosX(){
    	return this.posX;
    }
    public void setPosY(int y){
    	this.posY = y;
    }
    public int getPosY(){
    	return this.posY;
    }
    public void setMaxLadekapazitaet(int k){
    	this.maxLadekapazitaet = k;
    }
    public int getMaxLadekapazitaet(){
    	return this.maxLadekapazitaet;
    }
    public void setTyp(String t){
    	this.typ = t;
    }
    public String getTyp(){
    	return this.typ;
    }
    public void setAntrieb(String a){
    	this.antrieb = a;
    }
    public String getAntrieb(){
    	return this.antrieb;
    }
    public void setWinkel(int w){
    	this.winkel = w;
    }
    public int getWinkel(){
    	return this.winkel;
    }

	// Darstellung
	public static char[][] getDarstellung() {
		char[][] raumschiffShape = { 
				{'\0', '\0','_', '\0', '\0'},
				{'\0', '/', 'X', '\\', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'\0', '{', 'X', '}', '\0'},
				{'/', '_', '_','_', '\\'},				
		};
		return raumschiffShape;
	}

}
