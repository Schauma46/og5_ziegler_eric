package de.oszimt.starsim2099;

/**
 * Write a description of class Ladung here.
 * 
 * @author (Eric Ziegler)
 * @version (a version number or a date)
 */
public class Ladung {

	// Attribute
    private int posX;
    private int posY;
    private int masse;
    private String typ;
    
	// Methoden
    public Ladung(){
    	
    }
    public void setPosX(int x){
    	this.posX = x;
    }
    public int getPosX(){
    	return this.posX;
    }
    public void setPosY(int y){
    	this.posY = y;
    }
    public int getPosY(){
    	return this.posY;
    }
    public void setMasse(int m){
    	this.masse = m;
    }
    public int getMasse(){
    	return this.masse;
    }
    public void setTyp(String t){
    	this.typ = t;
    }
    public String getTyp(){
    	return this.typ;
    }
	// Darstellung
	public static char[][] getDarstellung() {
		char[][] ladungShape = { { '/', 'X', '\\' }, { '|', 'X', '|' }, { '\\', 'X', '/' } };
		return ladungShape;
	}
}