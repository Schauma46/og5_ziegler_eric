package raum;

public class Unterrichtsraum extends Raum {

	// Attribute
	private int schuelerplaetze;

	// Methoden
	public Unterrichtsraum() {

	}

	public Unterrichtsraum(int nummer, int schuelerplaetze) {
		super();
		this.schuelerplaetze = schuelerplaetze;
	}
	public void setSchuelerplaetze(int platz){
		this.schuelerplaetze = platz;
	}
	public int getSchuelerplaetze(){
		return this.schuelerplaetze;
	}
}
