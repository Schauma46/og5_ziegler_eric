package raum;

/**
RaumTest.java
Einfaches Beispiel f�r Vererbung
@author Ansorge, OSZ IMT
@version 1.0
*/

public class RaumTest {
public static void main (String[] args) {
  Raum r1;
  Unterrichtsraum r2;
  Fachraum r3;
  r1 = new Raum();
  r2 = new Unterrichtsraum();
  r3 = new Fachraum("3", 24, "Programmierungstechnik");
  r1.setNummer("1");
  r2.setNummer("2");
  r2.setSchuelerplaetze(24);
  r3.setSchuelerplaetze(14);
  System.out.println("+++++++++++ RaumTest ++++++++++++++");
  System.out.println("Raumnummer: "+r1.getNummer());
  System.out.println("Raumnummer: "+r2.getNummer()+
                     " ("+r2.getSchuelerplaetze()+" Schuelerplaetze)");
  System.out.println("Raumnummer: "+r3.getNummer()+
                     " ("+r3.getSchuelerplaetze()+" Schuelerplaetze)"+
                     " Fach: "+r3.getFach());
                     
} // main
} // RaumTest
