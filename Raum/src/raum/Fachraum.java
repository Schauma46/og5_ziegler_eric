package raum;

public class Fachraum extends Unterrichtsraum {

	// Attribute
	private String fach;

	// Methoden
	public Fachraum() {
		super();
	}

	public Fachraum(String nummer, int schuelerplaetze, String fach) {
		super();
		this.fach = fach;
	}
	public void setFach(String fach){
		this.fach = fach;
	}
	public String getFach(){
		return this.fach;
	}
}
