package raum;

public class Raum {

	// Attribute
	private String nummer;

	// Methoden
	public Raum() {

	}

	public Raum(String num) {
		this.nummer = num;
	}

	public void setNummer(String num) {
		this.nummer = num;
	}

	public String getNummer() {
		return this.nummer;
	}

}
