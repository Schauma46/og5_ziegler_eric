package tictactoe;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;

public class TicTacToe extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe frame = new TicTacToe();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public TicTacToe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(3, 3, 0, 0));
		
		JButton btnNewButton_1 = new JButton("");
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("");
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("");
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("");
		contentPane.add(btnNewButton_4);
		
		JButton btnNewButton_5 = new JButton("");
		contentPane.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("");
		contentPane.add(btnNewButton_6);
		
		JButton btnNewButton_7 = new JButton("");
		contentPane.add(btnNewButton_7);
		
		JButton btnNewButton_8 = new JButton("");
		contentPane.add(btnNewButton_8);
		
		JButton btnNewButton_9 = new JButton("");
		contentPane.add(btnNewButton_9);
	}

}
