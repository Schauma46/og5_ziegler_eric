package fanundverein;

/**
 * FanVereinTest.java
 * Einfaches Beispiel zu einer unidirektionalen Assoziation.
 * "Eine Fan unterstützt einen Verein"
 * @version 1.0 vom 12.01.2009
 * @author Ansorge, OSZ IMT
 */

public class FanVereinTest {

 private static void anzeigen(Fan f){
   String fanname = " LEER";
   String vereinsname = "niemand";
   if(f != null){
     fanname = f.getName();
     if(f.getVerein() != null)
       vereinsname = f.getVerein().getName();
   }
   System.out.println(fanname+" unterstuetzt "+vereinsname);
 } //anzeigen
 
 public static void main(String[] args) {
   Verein v1 = new Verein("Herta BSC");
   Verein v2 = new Verein("Bayern Muenchen");
   Fan f1 = new Fan("Anton");
   Fan f2 = new Fan("Emma");
   f1.setVerein(v1);
   anzeigen(f1);
   anzeigen(f2);
   f2.setVerein(v2);
   System.out.print("  nach f2.setVerein(v2) : ");
   anzeigen(f2);
   f1.removeVerein();
   System.out.print("  nach f1.removeVerein() : ");
   anzeigen(f1);
 } // main
 
} //FanVereinTest