package fanundverein;

public class Fan {

//Attribute
	private String name;
	private Verein verein;
	
//Methoden
	
	public Fan(String name){
		this.name = name;
	}
	public void setName(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
	public void setVerein(Verein verein){
		this.verein = verein;
	}
	public Verein getVerein(){
		return this.verein;
	}
	public void removeVerein(){
		
	}
}
