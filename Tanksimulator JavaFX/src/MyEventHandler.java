import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;


public class MyEventHandler implements EventHandler<ActionEvent>  {

	private TankSimulator f;

	public MyEventHandler(TankSimulator f) {
		this.f = f;
	}

	@Override
	public void handle(ActionEvent e) {

		// Hole Quelle
		Object obj = e.getSource();
		
		/* Verarbeite Events
		 *  - btnBeenden
		 *  - btnEinfuellen
		 *  - ...
		 */
		if (obj == f.btnBeenden)
			Platform.exit();
		
		if (obj == f.btnEinfuellen) {
			if (f.myTank.getFuellstand()< 95){
				double fuellstand = f.myTank.getFuellstand();
				fuellstand = fuellstand + 5;
				f.myTank.setFuellstand(fuellstand);
				f.pb.setProgress(fuellstand / 100);
				f.pi.setProgress(fuellstand / 100);
				
			}
			else {
				f.myTank.setFuellstand(100.0);
				f.pb.setProgress(1.0);
				f.pi.setProgress(1.0);
			} 
		}
		if (obj == f.btnVerbrauchen){
            if(f.myTank.getFuellstand()> f.slider.getValue()){
           	 double fuellstand = f.myTank.getFuellstand();
           	 fuellstand = fuellstand - f.slider.getValue();
           	 f.myTank.setFuellstand(fuellstand);
             f.pb.setProgress(fuellstand / 100);
             f.pi.setProgress(fuellstand / 100);
             
            }
            else{
           	 f.myTank.setFuellstand(0.0);
           	 f.pb.setProgress(0.0);
           	 f.pi.setProgress(0.0);
            }
		}
		
		if (obj == f.btnZuruecksetzen){
			f.myTank.setFuellstand(0.0);
			f.pb.setProgress(0.0);
			f.pi.setProgress(0.0);
		}
	}

}