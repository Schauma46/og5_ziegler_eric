package ZooTycoon;

public class Addon {

	//Attribute
	private double preis;
	private long idNummer;
	private String bezeichnung;
	private int bestand;
	private int maxBestand;
	private String artikel;
	
	//Standard-Konstruktor
	public Addon(){
		
	}
	
	//Parameteriesierter konstruktor
	public Addon(double preis,long idNummer, String bezeichnung, int bestand, int maxBestand, String artikel){
		this.preis = preis;
		this.idNummer = idNummer;
		this.bezeichnung = bezeichnung;
		this.bestand = bestand;
		this.maxBestand = maxBestand;
		this.artikel = artikel;
	}
	
	//getter und setter 
	public void setPreis(double p){
		this.preis = p;
	}
	public double getPreis(){
		return this.preis;
	}
	public void setIDNummer(long id){
		this.idNummer = id;
	}
	public double getIDNummer(){
		return this.idNummer;
	}
	public void setBezeichnung(String b){
		this.bezeichnung = b;
	}
	public String getBezeichnung(){
		return this.bezeichnung;
	}
	public void setBestand(int b){
		this.bestand = b;
	}
	public int getBestand(){
		return this.bestand;
	}
	public void setMaxBestand(int mb){
		this.maxBestand = mb;
	}
	public int getMaxBestand(){
		return this.maxBestand;
	}
	public void setArtikel(String a){
		this.artikel = a;
	}
	public String getArtikel(){
		return this.artikel;
	}
	
	//methoden
	public void Kaufen(){
		if(this.maxBestand <= 300){
			this.bestand += 5;
		}
		else{
			this.bestand = 300;
		}
	}
	public void Verbrauchen(){
		if(this.bestand <= 0){
			this.bestand -= 1;
		}
		else{
			this.bestand = 0;
		}
		
	}
	public String Achievment(String a){
		if(a.equals("Girafffutter")){
			if(this.bestand >= 250){
				return "Achievment Unlocked";
			}
			else{
				return "Nothing Unlocked";
			}
		}
		return "typ in an Artikel!";
	}
	public double gesamtwertBestand(){
		double gesamt = this.bestand * this.preis;
		return gesamt;
	}

	


}
