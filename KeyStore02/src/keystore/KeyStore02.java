package keystore;

import java.util.ArrayList;

public class KeyStore02 extends java.lang.Object {
	private ArrayList<String> keyliste;

	KeyStore02() {
		keyliste = new ArrayList<String>();
	}

	public void clear() {
		keyliste.clear();
	}

	public String getIndex(int index) {
		return keyliste.get(index);
	}

	public int indexOf(String e) {
		return keyliste.indexOf(e);
	}

	public void remove(int i) {
		keyliste.remove(i);
	}

	public void remove(String e) {
		keyliste.remove(e);
	}

	public int size() {
		return keyliste.size();
	}

	public boolean add(String e) {
		return keyliste.add(e);
	}

	public String toString() {
		return keyliste.toString();
	}
}
