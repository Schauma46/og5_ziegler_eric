package master;

import java.util.Scanner;

public class TaschenrechnerTest {

	public static void main(String[] args) {
		
		// TODO Auto-generated method stub
      
		Scanner scan = new Scanner(System.in);
		Taschenrechner ts = new Taschenrechner();

		int swValue;
        double eing1 = 0;
        double eing2 = 0;
        
        
        
		// Display menu graphics
		System.out.println("============================");
		System.out.println("|   MENU SELECTION DEMO    |");
		System.out.println("============================");
		System.out.println("| Options:                 |");
		System.out.println("|        1. Addieren       |");
		System.out.println("|        2. Subtrahieren   |");
		System.out.println("|        3. Multiplizieren |");
		System.out.println("|        4. Dividieren     |");
		System.out.println("|        5. Exit           |");
		System.out.println("============================");
		System.out.print(" Select option: ");
		swValue = scan.next().charAt(0);

		// Switch construct
		switch (swValue) {
		case '1':
			System.out.print("Bitte erste Zahl zum Addieren eingeben : ");
			eing1 = scan.nextDouble();
			System.out.print("Bitte zweite Zahl zum Addieren eingeben: ");
			eing2 = scan.nextDouble();
			System.out.println(eing1 + " + " + eing2 + " = " + ts.add(eing1, eing2));
			break;
		case '2':
			System.out.print("Bitte erste Zahl zum Subtrahieren eingeben   : ");
			eing1 = scan.nextDouble();
			System.out.print("Bitte zweite Zahl die abgezogen wird eingeben: ");
			eing2 = scan.nextDouble();
			System.out.println(eing1 + " - " + eing2 + " = " + ts.sub(eing1, eing2));
			break;	
		case '3':
			System.out.print("Bitte erste Zahl zum Multiplizieren eingeben : ");
			eing1 = scan.nextDouble();
			System.out.print("Bitte zweite Zahl zum Multiplizieren eingeben: ");
			eing2 = scan.nextDouble();
			System.out.println(eing1 + " * " + eing2 + " = " + ts.mul(eing1, eing2));
			break;
		case '4':
			System.out.print("Bitte erste Zahl zum Dividieren eingeben         : ");
			eing1 = scan.nextDouble();
			System.out.print("Bitte zweite Zahl durch die geteilt wird eingeben: ");
			eing2 = scan.nextDouble();
			System.out.println(eing1 + " / " + eing2 + " = " + ts.div(eing1, eing2));
			break;
		case '5':
			System.out.println("Danke f�r die benutzung des Taschenrechners!");
			System.exit(0);
			break;
		  
		  
		default:
			System.out.println("Invalid selection");
			break; // This break is not really necessary
		}
		scan.close();
        

	}
	
}
