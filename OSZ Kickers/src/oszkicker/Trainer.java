package oszkicker;

public class Trainer extends Person {

	// Attribute
	private char lizenzklasse;
	private double aufwandentschaedigung;

	// Methoden
	public Trainer() {
		super();
	}

	public void setLizenzklasse(char lizenz) {
		this.lizenzklasse = lizenz;
	}

	public char getLizenzklasse() {
		return this.lizenzklasse;
	}

	public void setAufwandentschaedigung(double entschaedigung) {
		this.aufwandentschaedigung = entschaedigung;
	}

	public double getAufwandentschaedigung() {
		return this.aufwandentschaedigung;
	}
}
