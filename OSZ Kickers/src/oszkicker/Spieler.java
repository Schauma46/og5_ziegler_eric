package oszkicker;

public class Spieler extends Person {

	// Attribute
	private int trikotnummer;
	private String spielerposition;
	private String mannschaftsname;
	private int mannschaft;

	// Methoden
	public Spieler() {
		super();
	}

	public void setTrikotnummer(int trikot) {
		this.trikotnummer = trikot;
	}

	public int getTrikotnummer() {
		return this.trikotnummer;
	}

	public void setSpielerposition(String position) {
		this.spielerposition = position;
	}

	public String getSpielerposition() {
		return this.spielerposition;
	}
	
	public void setMannschaftsname(String mname) {
		if(this.getMannschaft() <= 0){
			this.mannschaftsname = mname;
		}

	}

	public String getMannschaftsname() {
		return this.mannschaftsname;
	}
	
	public void setMannschaft(int m){
		this.mannschaft = m;
	}
	
	public int getMannschaft(){
		return this.mannschaft;
	}
}
