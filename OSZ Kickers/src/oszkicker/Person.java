package oszkicker;

public abstract class Person {

	// Attribute
	private String vorname;
	private String nachname;
	private String telNr;
	private boolean jahresbeitrag;

	// Methoden
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getVorname() {
		return this.vorname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getNachname() {
		return this.nachname;
	}

	public void setTelNr(String tel) {
		this.telNr = tel;
	}

	public String getTelNr() {
		return this.telNr;
	}

	public void setJahresbeitrag(boolean jahr) {
		this.jahresbeitrag = jahr;
	}

	public boolean getJahresbeitrag() {
		return this.jahresbeitrag;
	}
}