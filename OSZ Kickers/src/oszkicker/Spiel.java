package oszkicker;

public class Spiel {

	// Anfang Attribute
	private String heimmannschaft;
	private String gastmannschaft;
	private String datum;
	private int heimtore;
	private int gasttore;
	private int gelbekarte;
	// Ende Attribute

	// Anfang Methoden
	public Spiel() {

	}

	public String getHeimmannschaft() {
		return heimmannschaft;
	}

	public void setHeimmannschaft(String heimmannschaft) {
		this.heimmannschaft = heimmannschaft;
	}

	public String getGastmannschaft() {
		return gastmannschaft;
	}

	public void setGastmannschaft(String gastmannschaft) {
		this.gastmannschaft = gastmannschaft;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public int getHeimtore() {
		return heimtore;
	}

	public void setHeimtore(int heimtore) {
		this.heimtore = heimtore;
	}

	public int getGasttore() {
		return gasttore;
	}

	public void setGasttore(int gasttore) {
		this.gasttore = gasttore;
	}

	public int getGelbekarte() {
		return gelbekarte;
	}

	public void setGelbekarte(int gelbekarte) {
		this.gelbekarte = gelbekarte;
	}

}
