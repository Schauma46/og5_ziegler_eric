package oszkicker;

public class Mannschaft {

	// Anfang Attribute
	private String name;
	private String spielklasse;
	private int spieleranzahl;
	// Ende Attribute

	public Mannschaft() {

	}

	// Anfang Methoden
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpielklasse() {
		return spielklasse;
	}

	public void setSpielklasse(String spielklasse) {
		this.spielklasse = spielklasse;
	}

	public int getSpieleranzahl() {
		return spieleranzahl;
	}

	public void setSpieleranzahl(int spieleranzahl) {
		this.spieleranzahl = spieleranzahl;
	}

}
