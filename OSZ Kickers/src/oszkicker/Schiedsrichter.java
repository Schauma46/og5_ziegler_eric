package oszkicker;

public class Schiedsrichter extends Person {

//Attribute
	private int gepfiffeneSpiele;
//Methoden
	public Schiedsrichter(){
		super();
	}
	public void setGepfiffeneSpiele(int spiele){
		this.gepfiffeneSpiele = spiele;
	}
	public int getGepfiffeneSpiele(){
		return this.gepfiffeneSpiele;
	}
}
