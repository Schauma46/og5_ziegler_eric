package oszkicker;

public class OSZ_KickersTest {

	public static void main(String[] Args) {
		// Objekte erstellen
		Spieler spieler = new Spieler();
		Trainer trainer = new Trainer();
		Schiedsrichter schiedsrichter = new Schiedsrichter();
		Mannschaftsleiter leiter = new Mannschaftsleiter();

		// Objekt Spieler werte zuweisen
		spieler.setVorname("Eric");
		spieler.setNachname("Ziegler");
		spieler.setTelNr("0157 58091939");
		spieler.setJahresbeitrag(true);
		spieler.setTrikotnummer(19);
		spieler.setSpielerposition("Torwart");

		// Objekt trainer werte zuweisen
		trainer.setVorname("Kendrick");
		trainer.setNachname("Bollens");
		trainer.setTelNr("Schaut nie auf sein Handy");
		trainer.setJahresbeitrag(true);
		trainer.setLizenzklasse('A');
		trainer.setAufwandentschaedigung(50.00);

		// Objekt schiedsrichter werte zuweisen
		schiedsrichter.setVorname("Nikki");
		schiedsrichter.setNachname("Gro�");
		schiedsrichter.setTelNr("0146 55733488");
		schiedsrichter.setJahresbeitrag(false);
		schiedsrichter.setGepfiffeneSpiele(80);

		// Objekt leiter werte zuweisen
		leiter.setVorname("Bastian");
		leiter.setNachname("Pries");
		leiter.setTelNr("Hat das Handy immer aus");
		leiter.setJahresbeitrag(true);
		leiter.setTrikotnummer(9);
		leiter.setSpielerposition("St�rmer");
		leiter.setMannschaftsname("1.FC Zappel");
		leiter.setEngagementlvl(5);

		// Objekte testen
		System.out.println("|--------------------------------|");
		System.out.println("|         Objekt Spieler          ");
		System.out.println("|" + spieler.getVorname());
		System.out.println("|" + spieler.getNachname());
		System.out.println("|" + spieler.getTelNr());
		System.out.println("|" + spieler.getJahresbeitrag());
		System.out.println("|" + spieler.getTrikotnummer());
		System.out.println("|" + spieler.getSpielerposition());
		System.out.println("|--------------------------------|");
		System.out.println("");
		System.out.println("|--------------------------------|");
		System.out.println("|         Objekt Trainer          ");
		System.out.println("|" + trainer.getVorname());
		System.out.println("|" + trainer.getNachname());
		System.out.println("|" + trainer.getTelNr());
		System.out.println("|" + trainer.getJahresbeitrag());
		System.out.println("|" + trainer.getAufwandentschaedigung());
		System.out.println("|" + trainer.getLizenzklasse());
		System.out.println("|--------------------------------|");
		System.out.println("");
		System.out.println("|--------------------------------|");
		System.out.println("|      Objekt Schiedsrichter      ");
		System.out.println("|" + schiedsrichter.getVorname());
		System.out.println("|" + schiedsrichter.getNachname());
		System.out.println("|" + schiedsrichter.getTelNr());
		System.out.println("|" + schiedsrichter.getJahresbeitrag());
		System.out.println("|" + schiedsrichter.getGepfiffeneSpiele());
		System.out.println("|--------------------------------|");
		System.out.println("");
		System.out.println("|--------------------------------|");
		System.out.println("|     Objekt Mannschaftsleiter    ");
		System.out.println("|" + leiter.getVorname());
		System.out.println("|" + leiter.getNachname());
		System.out.println("|" + leiter.getTelNr());
		System.out.println("|" + leiter.getJahresbeitrag());
		System.out.println("|" + leiter.getMannschaftsname());
		System.out.println("|" + leiter.getEngagementlvl());
		System.out.println("|--------------------------------|");

	}
}
