package oszkicker;

public class Mannschaftsleiter extends Spieler {

	// Attribute
	private int engagementlvl;

	// Methoden
	public Mannschaftsleiter() {
		super();
	}

	public void setEngagementlvl(int lvl) {
		this.engagementlvl = lvl;
	}

	public int getEngagementlvl() {
		return this.engagementlvl;
	}
}
