package klausur;

import java.util.Random;
import java.util.Scanner;

public class Klausur2 {

	public final int NICHT_GEFUNDEN = -1;
	private int vergleichL = 0;
	private int vergleichS = 0;
	private int vergleichB = 0;

	public static void main(String[] args) {
		Klausur2 k2 = new Klausur2();
		int n = 1000; // Anzahl der Stellen

		// Erstellt und sortiert Array mit n Stellen
		long[] zahlen = k2.getSortedIntArray(n);

		// ----------Hier in die Main k�nnen Sie Ihre Analyse f�r das Protokoll
		// schreiben--------------
		String auswahl = "";
		int gesuchteZahl = 0;

		Scanner scan = new Scanner(System.in);

		
		 
		System.out.println("(L)ineare, (S)chlumpf, (B)in�rSuche oder (A)lle?");
		auswahl = scan.next();
		if (auswahl.equals("l") || auswahl.equals("L")) {
			System.out.println("Gesuchtezahl: ");
			gesuchteZahl = scan.nextInt();
			System.out.println("Linear = " + k2.lineareSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheL());
		}
		if (auswahl.equals("s") || auswahl.equals("S")) {
			System.out.println("Gesuchtezahl: ");
			gesuchteZahl = scan.nextInt();
			System.out.println("Bin�r = " + k2.schlaubiSchlumpfSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheS());
		}
		if (auswahl.equals("b") || auswahl.equals("B")) {
			System.out.println("Gesuchtezahl: ");
			gesuchteZahl = scan.nextInt();
			System.out.println("Bin�r = " + k2.binaereSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheB());

		}
		if (auswahl.equals("a") || auswahl.equals("A")) {
			System.out.println("Gesuchtezahl: ");
			gesuchteZahl = scan.nextInt();
			System.out.println("Linear = " + k2.lineareSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheL());
			System.out.println("");
			System.out.println("Schlumpf = " + k2.schlaubiSchlumpfSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheS());
			System.out.println("");
			System.out.println("Bin�r = " + k2.binaereSuche(zahlen, gesuchteZahl));
			System.out.println("Vergleiche = " + k2.getVergleicheB());
		}
	}

	public int lineareSuche(long[] zahlen, long gesuchteZahl) {
		for (int i = 0; i < zahlen.length; i++) {
			vergleichL++;
			if (zahlen[i] == gesuchteZahl) {
				
				return i;
			}
		}
		return NICHT_GEFUNDEN;
	}

	public int schlaubiSchlumpfSuche(long[] zahlen, long gesuchteZahl) {
		int l = zahlen.length / 4 - 1;
		while (l < zahlen.length && l >= 0) {
			vergleichS++;
			if (gesuchteZahl == zahlen[l]) {
				vergleichS++;
				return l;
			}
			if (gesuchteZahl < zahlen[l]) {
				vergleichS++;
				l--;
			} else {
				vergleichS++;
				l = l + zahlen.length / 4;
			}
		}
		return NICHT_GEFUNDEN;
	}

	public int binaereSuche(long[] zahlen, long gesuchteZahl) {
		int l = 0, r = zahlen.length - 1;
		int m;

		while (l <= r) {
			vergleichB++;
			// Bereich halbieren
			m = l + ((r - l) / 2);

			if (zahlen[m] == gesuchteZahl) { // Element gefunden?
				vergleichB++;
				return m;
			}

			if (zahlen[m] > gesuchteZahl) {
				vergleichB++;
				r = m - 1; // im linken Abschnitt weitersuchen
			} else {
				vergleichB++;
				l = m + 1; // im rechten Abschnitt weitersuchen
			}
		}
		return NICHT_GEFUNDEN;
	}

	// -----------------------Finger weg von diesem Teil des Codes

	/**
	 * Methode f�llt das Attribut zahlen mit einem Array der L�nge des
	 * Paramaters z.B. getIntArray(1000) gibt ein 1000-stelliges Array zur�ck
	 * 
	 * @param stellen
	 *            - Anzahl der zuf�lligen Stellen
	 * @return das Array
	 */
	public long[] getSortedIntArray(int stellen) {
		Random rand = new Random(666L);
		long[] zahlen = new long[stellen];
		for (int i = 0; i < stellen; i++)
			zahlen[i] = rand.nextInt(20000000);
		this.radixSort(zahlen);
		return zahlen;
	}

	/**
	 * Super effizienter Algorithmus zur Sortierung der Zahlen Laufzeit von
	 * //NoHintAvailable
	 */
	public void radixSort(long[] zahlen) {
		long[] temp = new long[20000000];
		for (long z : zahlen)
			temp[(int) z]++;
		int stelle = 0;
		for (int i = 0; i < temp.length; i++)
			if (temp[i] != 0)
				while (temp[i]-- != 0)
					zahlen[stelle++] = i;
	}

	public int getVergleicheL() {
		return this.vergleichL;
	}

	public int getVergleicheS() {
		return this.vergleichS;
	}

	public int getVergleicheB() {
		return this.vergleichB;
	}

}
