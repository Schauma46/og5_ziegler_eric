package tagebuch;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileControl {
	private List<LernEintrag> lerneintrag = new ArrayList<LernEintrag>();
	public File file = new File("Eric.dat");
	String Name = "";
	
	public void DateiEinlesen(File file){
		try{
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		
		Date date = null;
		String fach = "";
		String beschreibung = "";
		int dauer = 0;
		DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
		
		String s = br.readLine();
		while(s != null){
			do{
			s = br.readLine();
			if(s == null)
				break;
			date = df.parse(s);
			s = br.readLine();
			fach = s;
			s = br.readLine();
			beschreibung = s;
			s = br.readLine();
			dauer = Integer.parseInt(s);
			LernEintrag lern = new LernEintrag(date, fach, beschreibung, dauer);
			
			lerneintrag.add(lern);
			}while(s != "");
		}
		System.out.println(lerneintrag);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public List<LernEintrag> getLern() {
		return this.lerneintrag;
	}

	public void setLern(List<LernEintrag> lern) {
		this.lerneintrag = lern;
	}
	
	public String Lernername() throws IOException{
		File file1 = new File("Eric.dat");
		FileReader fr = new FileReader(file1);
		BufferedReader br = new BufferedReader(fr);
		Name = br.readLine();
		return Name;
	}
	
	public void Einlesen(Date date, String fach, String beschreibung, int dauer) throws IOException{

		LernEintrag lernen = new LernEintrag(date, fach, beschreibung, dauer);
		
		lerneintrag.add(lernen);
		
		Writer output;
		output = new BufferedWriter(new FileWriter(file, true));  
		output.append(lernen.toString());
		output.close();

	}
}
