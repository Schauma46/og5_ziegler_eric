package tagebuch;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.Date; 

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class LernTagebuchGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LernTagebuchGUI frame = new LernTagebuchGUI(new Logic(new FileControl()));
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Logic L;
	private JTable table;
	
	public LernTagebuchGUI(Logic L) throws IOException{
		L.fc.DateiEinlesen(L.fc.file);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 572, 294);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_lbl = new JPanel();
		contentPane.add(panel_lbl, BorderLayout.NORTH);
		panel_lbl.setLayout(new BorderLayout(0, 0));
		
		JLabel lblNewLabel = new JLabel("Lerntagebuch von ");
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 22));
		panel_lbl.add(lblNewLabel, BorderLayout.WEST);
		
		JLabel lblNewLabel_3 = new JLabel(L.getLernerName());
		lblNewLabel_3.setFont(new Font("Comic Sans MS", Font.PLAIN, 22));
		panel_lbl.add(lblNewLabel_3, BorderLayout.CENTER);
		
		JPanel panel_south = new JPanel();
		contentPane.add(panel_south, BorderLayout.SOUTH);
		panel_south.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_btns = new JPanel();
		panel_south.add(panel_btns, BorderLayout.EAST);
		panel_btns.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton btnNewButton = new JButton("Neuer Eintrag...");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				NewEntrie E = new NewEntrie(new Logic(new FileControl()));
				E.setVisible(true);
			}
		});
		panel_btns.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Bericht...");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				Bericht B = new Bericht(new Logic(new FileControl()));
				B.setVisible(true);
				}catch(Exception ee){
					ee.printStackTrace();
				}
			}
		});
		
		JButton btnNewButton_3 = new JButton("Reload");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				try {
					LernTagebuchGUI LTb = new LernTagebuchGUI(new Logic(new FileControl()));
					//L.fc.DateiEinlesen(L.fc.file);
					LTb.setVisible(true);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				
			}
		});
		panel_btns.add(btnNewButton_3);
		panel_btns.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Beenden");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		panel_btns.add(btnNewButton_2);
		
		JPanel panel_Center = new JPanel();
		contentPane.add(panel_Center, BorderLayout.CENTER);
		panel_Center.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_head = new JPanel();
		panel_head.setBackground(Color.WHITE);
		panel_head.setForeground(Color.WHITE);
		panel_Center.add(panel_head, BorderLayout.NORTH);
		panel_head.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_Center.add(scrollPane, BorderLayout.CENTER);
		
		String[] s = {"Datum","Fach","Dauer","Beschreibung"};
		String[][] b = L.list();
		
		table = new JTable(b,s);
		scrollPane.setViewportView(table);
	
	}

}
