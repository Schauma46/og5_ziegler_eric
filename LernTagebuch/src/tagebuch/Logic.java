package tagebuch;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Logic{

	FileControl fc = new FileControl();
	Logic(FileControl f){
		fc = f;
	}

	
	
	public String[][] list(){
		List<LernEintrag> lern = fc.getLern();
		String[][] temp = new String[lern.size()][4];
		for(int i = 0; i < lern.size(); i++){
			LernEintrag buch = lern.get(i);
			DateFormat df = new SimpleDateFormat("dd.mm.yyyy");
			temp[i][0] = df.format(buch.getDatum());
			temp[i][1] = buch.getFach();
			temp[i][2] = "" + buch.getDauer();
			temp[i][3] = buch.getBeschreibung();
		}
		
		return temp;
		
	}
	
	public int getAnzahlList(){
		List<LernEintrag> lern = fc.getLern();
		return lern.size();
				
	}
	
	public int durchschnittGelernt(){
		List<LernEintrag> lern = fc.getLern();
		int ds = 0;
		int zaehler = 0;
		for(int i = 0; i < lern.size(); i++){
			LernEintrag buch = lern.get(i);
			ds += buch.getDauer();
			zaehler++;
		}
		
		return ds / zaehler;
	}
	
	public int insgesamtGelernt(){
		List<LernEintrag> lern = fc.getLern();
		int all = 0;
		for(int i = 0; i < lern.size(); i++){
			LernEintrag buch = lern.get(i);
			all += buch.getDauer();
		}
		
		return all;
	}

	public String getLernerName() throws IOException {
		
		return fc.Lernername();
	}
	
}
