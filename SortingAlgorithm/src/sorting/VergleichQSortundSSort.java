package sorting;

import java.util.Random;
import java.util.Scanner;

public class VergleichQSortundSSort {

	public static void main(String[] args) {

		int arrayl;
		long a = 0;
		String auswahl = "";
		String ausfuerlich = "j";
		String wiederholen = "j";
		
		Scanner scan = new Scanner(System.in);
		Stoppuhr Stoppuhr = new Stoppuhr();
		QuickSortAusfuerlich QuickSortA = new QuickSortAusfuerlich();
		QuickSort QuickSort = new QuickSort();
		SelectSortAusfuehrlich SelectSortA = new SelectSortAusfuehrlich();
		SelectSort SelectSort = new SelectSort();
		
		
		System.out.println("Willkommen beim Vergleich vom QuickSort mit dem SelectSort!");
		System.out.println("Bitte geben Sie zuerst die L�nge des unsortierten Arrays an!");
		arrayl = scan.nextInt();
		Random rand = new Random(11);
		long[] test = new long[20];
		for (int i = 0; i < test.length; i++) {
			a = rand.nextInt(1111) + 1;
			test[i] = a;
		}
		while(wiederholen.equals("j")){
		System.out.println("Dr�cken Sie 'q' f�r QuickSort oder 's' f�r SelectSort!");
		auswahl = scan.next();
		if(auswahl.equals("q")){
			System.out.println("Wollen sie Quicksort in der Ausf�hrlichen variante? Dann dr�cke 'j' ansonsten 'n'!");
			ausfuerlich = scan.next();
			if(ausfuerlich.equals("j")){
				System.out.println("unsortierte Liste: " + QuickSortA.Array2Str(test));
				Stoppuhr.Starten();
				QuickSortA.sortiere(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + QuickSortA.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + QuickSortA.getVertauschungen() + " mal getauscht!");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			}
			if(ausfuerlich.equals("n")) {
				System.out.println("unsortierte Liste: " + QuickSort.Array2Str(test));
				Stoppuhr.Starten();
				QuickSort.sortiere(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + QuickSort.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + QuickSort.getVertauschungen() + " mal getauscht!");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			}
			else{
				System.out.println("Falsche Eingabe! Nochmal? Wenn ja dann 'j' dr�cken");
				wiederholen = scan.next();
			}
		}
		else if(auswahl.equals("s")){
			System.out.println("Wollen sie Selectsort in der Ausf�hrlichen variante? Dann dr�cke 'j' ansonsten 'n'!");
			auswahl = scan.next();
			if (ausfuerlich.equals("j") || auswahl.equals("J")) {
				System.out.println("unsortierte Liste: " + SelectSortA.Array2Str(test));
				Stoppuhr.Starten();
				SelectSortA.SSort(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + SelectSortA.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + SelectSortA.getVertauschungen() + " mal getauscht!");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			} 
			if(ausfuerlich.equals("n")){
				System.out.println("unsortierte Liste: " + SelectSort.Array2Str(test));
				Stoppuhr.Starten();
				SelectSort.SSort(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + SelectSort.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + SelectSort.getVertauschungen() + " mal getauscht!");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			}
			else {
			System.out.println("Falsche Eingabe! Nochmal? Wenn ja dann 'j' dr�cken");
			wiederholen = scan.next();
		    }
		}
		else {
			System.out.println("Falsche Eingabe! Nochmal? Wenn ja dann 'j' dr�cken");
			wiederholen = scan.next();
		    }
		
		}
	}
}
