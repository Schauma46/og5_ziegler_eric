package sorting;

import java.util.Random;
import java.util.Scanner;

public class SelectSortTest {

	public static void main(String[] args) {
		long a = 0;
		int arrayl = 0;
		String auswahl = "";
		String wiederholen = "j";

		SelectSortAusfuehrlich SelectSortA = new SelectSortAusfuehrlich();
		SelectSort SelectSort = new SelectSort();
		Stoppuhr Stoppuhr = new Stoppuhr();
		Scanner scan = new Scanner(System.in);

		while (wiederholen.equals("j")) {
			System.out.println("Geben Sie die L�nge des Arrays an!");
			arrayl = scan.nextInt();
			Random rand = new Random(11);
			long[] test = new long[arrayl];
			for (int i = 0; i < test.length; i++) {
				a = rand.nextInt(11111) + 1;
				test[i] = a;
			}
			System.out.println("Wollen sie Selectsort in der Ausf�hrlichen variante? Dann dr�cke 'j' ansonsten 'n'!");
			auswahl = scan.next();
			if (auswahl.equals("j") || auswahl.equals("J")) {
				System.out.println("unsortierte Liste: " + SelectSortA.Array2Str(test));
				Stoppuhr.Starten();
				SelectSortA.SSort(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + SelectSortA.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + SelectSortA.getVertauschungen() + " mal getauscht!");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			} else {
				System.out.println("unsortierte Liste: " + SelectSort.Array2Str(test));
				Stoppuhr.Starten();
				SelectSort.SSort(test);
				Stoppuhr.Stoppen();
				System.out.println("sortierte Liste: " + SelectSort.Array2Str(test));
				System.out.println("");
				System.out.println("Es wurde " + SelectSort.getVertauschungen() + " mal getauscht!");
				System.out.println("");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
				System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
				System.out.println("");
			}
			System.out.println("nochmal? Wenn ja dann 'j' dr�cken");
			wiederholen = scan.next();
		}

	}

}
