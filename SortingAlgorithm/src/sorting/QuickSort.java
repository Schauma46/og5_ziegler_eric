package sorting;

public class QuickSort {

	private int vertauschungen = 0;

	public void sortiere(long[] array) {
		QSort(array, 0, array.length - 1);
	}

	public void QSort(long[] array, int links, int rechts) {
		if (links < rechts) {
			int i = partition(array, links, rechts);
			QSort(array, links, i - 1);
			QSort(array, i + 1, rechts);
		}
	}

	public int partition(long[] array, int links, int rechts) {
		long pivot;
		int i;
		int j;
		long temp;
		pivot = array[rechts];
		i = links;
		j = rechts - 1;
		while (i <= j) {
			if (array[i] > pivot) {
				temp = array[i];
				array[i] = array[j];
				array[j] = temp;
				j--;
				vertauschungen++;
			} else
				i++;
		}
		temp = array[i];
		array[i] = array[rechts];
		array[rechts] = temp;

		return i;
	}

	public int getVertauschungen() {
		return this.vertauschungen;
	}

	public static String Array2Str(long[] array) {
		String back = "";
		for (int i = 0; i < array.length - 1; i++) {
			back = back + array[i] + ", ";
		}

		return back;
	}
}