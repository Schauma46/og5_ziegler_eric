package sorting;

public class SelectSortAusfuehrlich {

	private int vertauschungen = 0;

	public long[] SSort(long[] array) {

		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					System.out.println("Es soll die gr��ere (" + array[i] + ") mit der kleineren Zahl (" + array[j] + ") getauscht werden!");
					System.out.print("gr��ere Zahl (" + array[i] + ") zu temp -> ");
					long temp = array[i];
					System.out.print("kleinere Zahl (" + array[j] + ") auf Stelle von der gr��eren -> ");
					array[i] = array[j];
					System.out.println("temp (" + temp + ") auf Stelle von der kleineren");
					System.out.println("");
					array[j] = temp;
					vertauschungen++;
				}
			}
		}

		return array;
	}

	public int getVertauschungen() {
		return this.vertauschungen;
	}

	public static String Array2Str(long[] array) {
		String back = "";
		for (int i = 0; i < array.length; i++) {
			back = back + array[i] + ", ";
		}

		return back;
	}

}