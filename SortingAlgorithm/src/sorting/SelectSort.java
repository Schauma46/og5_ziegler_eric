package sorting;

public class SelectSort {

	private int vertauschungen = 0;

	public long[] SSort(long[] array) {

		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					long temp = array[i];
					array[i] = array[j];
					array[j] = temp;
					vertauschungen++;
				}
			}
		}

		return array;
	}

	public int getVertauschungen() {
		return this.vertauschungen;
	}

	public static String Array2Str(long[] array) {
		String back = "";
		for (int i = 0; i < array.length; i++) {
			back = back + array[i] + ", ";
		}

		return back;
	}

}