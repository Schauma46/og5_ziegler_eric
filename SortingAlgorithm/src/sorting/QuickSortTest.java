package sorting;

import java.util.Random;

import java.util.Scanner;
public class QuickSortTest {

	public static void main(String[] args) {
		
		long a = 0;
		int arrayl = 0;
		String auswahl = "";
		String wiederholen = "j";

		Scanner scan = new Scanner(System.in);
		QuickSortAusfuerlich QuickSortA = new QuickSortAusfuerlich();
		QuickSort QuickSort = new QuickSort();
		Stoppuhr Stoppuhr = new Stoppuhr();
		
		while(wiederholen.equals("j")){
		System.out.println("Geben Sie die L�nge des Arrays an!");
		arrayl = scan.nextInt();
		Random rand = new Random(11);
		long[] test = new long[arrayl];
		for (int i = 0; i < test.length; i++) {
			a = rand.nextInt(1111) + 1;
			test[i] = a;
		}
		System.out.println("Wollen sie Quicksort in der Ausf�hrlichen variante? Dann dr�cke 'j' ansonsten 'n'!");
		auswahl = scan.next();
		if(auswahl.equals("j") || auswahl.equals("J")){
			System.out.println("unsortierte Liste: " + QuickSortA.Array2Str(test));
			Stoppuhr.Starten();
			QuickSortA.sortiere(test);
			Stoppuhr.Stoppen();
			System.out.println("sortierte Liste: " + QuickSortA.Array2Str(test));
			System.out.println("");
			System.out.println("Es wurde " + QuickSortA.getVertauschungen() + " mal getauscht!");
			System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
			System.out.println("");
			System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
			System.out.println("");
		}
		else {
			System.out.println("unsortierte Liste: " + QuickSort.Array2Str(test));
			Stoppuhr.Starten();
			QuickSort.sortiere(test);
			Stoppuhr.Stoppen();
			System.out.println("sortierte Liste: " + QuickSort.Array2Str(test));
			System.out.println("");
			System.out.println("Es wurde " + QuickSort.getVertauschungen() + " mal getauscht!");
			System.out.println("");
			System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() + " millisec gebraucht");
			System.out.println("Die Schleife hat: " + Stoppuhr.getDauer() / 1000.0 + " sekunden gebraucht");
			System.out.println("");
		}
		System.out.println("nochmal? Wenn ja dann 'j' dr�cken");
		wiederholen = scan.next();
	}
  
  }		
		
}
